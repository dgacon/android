package com.example.dgacon.android_project_gacon;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;

import java.util.Random;


public abstract class BitmapModifier {

    public static Bitmap toGrayscale(Bitmap bmp)
    {
        Bitmap bmpResult = bmp.copy(Bitmap.Config.ARGB_8888, true);
        int totalSize = bmpResult.getWidth() * bmpResult.getHeight();
        int[] pixs = new int[totalSize];
        bmpResult.getPixels(pixs,0,bmpResult.getWidth(),0,0,bmpResult.getWidth(),bmpResult.getHeight());
        for(int i = 0; i < totalSize; i++)
        {
            int p = pixs[i];
            int red = Color.red(p);
            int green = Color.green(p);
            int blue = Color.blue(p);
            int alpha = Color.alpha(p);

            int newred, newblue, newgreen;

            int grey = (red + green + blue) / 3;
            newred = grey;
            newblue = grey;
            newgreen = grey;

            if(newred > 255)
                newred = 255;
            if(newblue > 255)
                newblue = 255;
            if(newgreen > 255)
                newgreen = 255;
            if(newred < 0)
                newred = 0;
            if(newblue < 0)
                newblue = 0;
            if(newgreen < 0)
                newgreen = 0;
            pixs[i] = Color.argb(alpha,newred,newgreen,newblue);
        }
        bmpResult.setPixels(pixs,0,bmpResult.getWidth(),0,0,bmpResult.getWidth(), bmpResult.getHeight());
        return bmpResult;
    }

    public static Bitmap toColorize(Bitmap bmp)
    {
        Bitmap bmpResult = bmp.copy(Bitmap.Config.ARGB_8888, true);

        int totalSize = bmpResult.getWidth() * bmpResult.getHeight();
        int[] pixs = new int[totalSize];
        bmpResult.getPixels(pixs,0,bmpResult.getWidth(),0,0,bmpResult.getWidth(),bmpResult.getHeight());

        Random rand = new Random();
        float r = (float)(rand.nextInt(999))/1000;
        float g = (float)(rand.nextInt(999))/1000;
        float b = (float)(rand.nextInt(999))/1000;
        int rgb = rand.nextInt(3);

        for(int i = 0; i < totalSize; i++)
        {
            int p = pixs[i];
            int red = Color.red(p);
            int green = Color.green(p);
            int blue = Color.blue(p);
            int alpha = Color.alpha(p);
            int newred, newblue, newgreen;

            switch(rgb){
                case 0:
                    newred = (int)((red * r) + (green * g) + (blue * b));
                    newgreen = green;
                    newblue = blue;
                    break;
                case 1:
                    newred = red;
                    newgreen = (int)((red * r) + (green * g) + (blue * b));
                    newblue = blue;
                    break;
                case 2:
                    newred = red;
                    newgreen = green;
                    newblue = (int)((red * r) + (green * g) + (blue * b));
                    break;
                default:
                    newred = red;
                    newgreen = green;
                    newblue = blue;
                    break;
            }

            if(newred > 255)
                newred = 255;
            if(newblue > 255)
                newblue = 255;
            if(newgreen > 255)
                newgreen = 255;
            if(newred < 0)
                newred = 0;
            if(newblue < 0)
                newblue = 0;
            if(newgreen < 0)
                newgreen = 0;
            pixs[i] = Color.argb(alpha,newred,newgreen,newblue);
        }
        bmpResult.setPixels(pixs,0,bmpResult.getWidth(),0,0,bmpResult.getWidth(), bmpResult.getHeight());
        return bmpResult;
    }

    public static Bitmap Luminosity(Bitmap bmp, int value)
    {
        Bitmap bmpResult = bmp.copy(Bitmap.Config.ARGB_8888, true);
        int totalSize = bmpResult.getWidth() * bmpResult.getHeight();
        int[] pixs = new int[totalSize];
        bmpResult.getPixels(pixs,0,bmpResult.getWidth(),0,0,bmpResult.getWidth(),bmpResult.getHeight());
        for(int i = 0; i < totalSize; i++)
        {
            int p = pixs[i];
            int red = Color.red(p) + value;
            int green = Color.green(p)+ value;
            int blue = Color.blue(p)+ value;
            int alpha = Color.alpha(p);
            if(red > 255)
                red = 255;
            if(blue > 255)
                blue = 255;
            if(green > 255)
                green = 255;
            if(red < 0)
                red = 0;
            if(blue < 0)
                blue = 0;
            if(green < 0)
                green = 0;
            pixs[i] = Color.argb(alpha,red,green,blue);
        }
        bmpResult.setPixels(pixs,0,bmpResult.getWidth(),0,0,bmpResult.getWidth(), bmpResult.getHeight());
        return bmpResult;
    }

    public static Bitmap toKeep (Bitmap bmp)
    {
        Bitmap bmpResult = bmp.copy(Bitmap.Config.ARGB_8888, true);
        int totalSize = bmpResult.getWidth() * bmpResult.getHeight();
        int[] pixs = new int[totalSize];
        bmpResult.getPixels(pixs,0,bmpResult.getWidth(),0,0,bmpResult.getWidth(),bmpResult.getHeight());

        Random rand = new Random();
        int rgb = rand.nextInt(3);

        for(int i = 0; i < totalSize; i++)
        {
            int p = pixs[i];
            int red = Color.red(p);
            int green = Color.green(p);
            int blue = Color.blue(p);
            int alpha = Color.alpha(p);

            int newred, newblue, newgreen;

            int grey = (red + green + blue) / 3;

            switch(rgb){
                case 0:
                    newred = red;
                    newgreen = grey;
                    newblue = grey;
                    break;
                case 1:
                    newred = grey;
                    newgreen = green;
                    newblue = grey;
                    break;
                case 2:
                    newred = grey;
                    newgreen = grey;
                    newblue = blue;
                    break;
                default:
                    newred = grey;
                    newgreen = grey;
                    newblue = grey;
                    break;
            }

            if(newred > 255)
                newred = 255;
            if(newblue > 255)
                newblue = 255;
            if(newgreen > 255)
                newgreen = 255;
            if(newred < 0)
                newred = 0;
            if(newblue < 0)
                newblue = 0;
            if(newgreen < 0)
                newgreen = 0;
            pixs[i] = Color.argb(alpha,newred,newgreen,newblue);
        }
        bmpResult.setPixels(pixs,0,bmpResult.getWidth(),0,0,bmpResult.getWidth(), bmpResult.getHeight());
        return bmpResult;
    }

    public static Bitmap equalizeHistogram(Bitmap bmp)
    {
        Bitmap bmpResult = bmp.copy(Bitmap.Config.ARGB_8888, true);
        int totalSize = bmpResult.getWidth() * bmpResult.getHeight();
        int[] pixs = new int[totalSize];
        float[][] pixsHSV = new float[totalSize][3];
        bmpResult.getPixels(pixs,0,bmpResult.getWidth(),0,0,bmpResult.getWidth(),bmpResult.getHeight());
        float[] pixHSV = new float[3];
        int[] histo = new int[256];

        for(int i = 0; i < 256; i++)
            histo[i] = 0;

        for(int i = 0; i < totalSize; i++) {
            Color.colorToHSV(pixs[i], pixHSV);
            pixsHSV[i][0] = pixHSV[0]; //Hue
            pixsHSV[i][1] = pixHSV[1]; //Saturation
            pixsHSV[i][2] = pixHSV[2]; //Value

            histo[(int)(pixHSV[2] * 255)] += 1;
        }

        for(int i = 1; i < 256; i++)
            histo[i] += histo[i-1];


        for(int i = 0; i < totalSize; i++) {
            pixsHSV[i][2] =  histo[(int)(pixHSV[2] * 255)] * 255 / totalSize;
            pixs[i] = Color.HSVToColor(pixsHSV[i]);
        }

        bmpResult.setPixels(pixs,0,bmpResult.getWidth(),0,0,bmpResult.getWidth(), bmpResult.getHeight());
        return bmpResult;
    }

    public static Bitmap convolution(Bitmap bmp, float[][] matrice, int size) {
        Bitmap bmpResult = bmp.copy(Bitmap.Config.ARGB_8888, true);
        int width = bmpResult.getWidth();
        int height = bmpResult.getHeight();
        int totalSize =  width * height;
        int marge = size / 2;
        int[] pixs = new int[totalSize];
        bmpResult.getPixels(pixs,0,bmpResult.getWidth(),0,0,bmpResult.getWidth(),bmpResult.getHeight());
        for(int i = marge * width + marge; i < totalSize - (marge * width + marge); i++)
        {
            if(i % width <= marge)
                continue;
            if(i % width > width - marge)
                continue;
            int pix;
            int vRed = 0;
            int vGreen = 0;
            int vBlue = 0;
            for(int j = 0; j < size; j++)
                for(int k = 0; k < size; k++)
                {
                    //Matrice management
                    pix = pixs[(i - (marge * width + marge)) + j*width + k];
                    vRed +=(int)(Color.red(pix) * matrice[j][k]);
                    vGreen +=(int)(Color.green(pix) * matrice[j][k]);
                    vBlue +=(int)(Color.blue(pix) * matrice[j][k]);
                }
            pixs[i] = Color.argb(Color.alpha(pixs[i]), vRed, vGreen, vBlue);
        }
        bmpResult.setPixels(pixs,0,bmpResult.getWidth(),0,0,bmpResult.getWidth(), bmpResult.getHeight());
        return bmpResult;
    }
}
