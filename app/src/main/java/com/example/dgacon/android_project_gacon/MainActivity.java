package com.example.dgacon.android_project_gacon;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private Bitmap bitmap_original = null;
    private Bitmap bitmap = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bitmap_original = BitmapFactory.decodeResource(getResources(), R.drawable.fruits);
        bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.fruits);

        ImageView image = (ImageView) findViewById(R.id.image);
        image.setImageBitmap(bitmap);

        final Button button_reset = findViewById(R.id.button_reset);
        button_reset.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                bitmap = bitmap_original;
                refreshView();
            }
        });

        final Button button_grey = findViewById(R.id.button_grey);
        button_grey.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                bitmap = BitmapModifier.toGrayscale(bitmap);
                refreshView();
            }
        });

        final Button button_colorize = findViewById(R.id.button_colorize);
        button_colorize.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                bitmap = BitmapModifier.toColorize(bitmap);
                refreshView();
            }
        });

        final Button button_keep = findViewById(R.id.button_keep);
        button_keep.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                bitmap = BitmapModifier.toKeep(bitmap);
                refreshView();
            }
        });

        final Button button_brighter = findViewById(R.id.button_brighter);
        button_brighter.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Random rand = new Random();
                int n = rand.nextInt(255);
                bitmap = BitmapModifier.Luminosity(bitmap, n);
                refreshView();
            }
        });

        final Button button_darker = findViewById(R.id.button_darker);
        button_darker.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Random rand = new Random();
                int n = 0 - (rand.nextInt(255));
                bitmap = BitmapModifier.Luminosity(bitmap, n);
                refreshView();
            }
        });

        final Button button_equalize = findViewById(R.id.button_equalize);
        button_equalize.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                bitmap = BitmapModifier.equalizeHistogram(bitmap);
                refreshView();
            }
        });

        final Button button_convolution_3x3 = findViewById(R.id.button_convolution_3x3);
        button_convolution_3x3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final float[][] gauss = {{1.f/16, 1.f/8, 1.f/16},
                        {1.f/8, 1.f/4, 1.f/8},
                        {1.f/16, 1.f/8, 1.f/16}};
                bitmap = BitmapModifier.convolution(bitmap, gauss, 3);
                refreshView();
            }
        });

        final Button button_convolution_5x5 = findViewById(R.id.button_convolution_5x5);
        button_convolution_5x5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final float[][] gauss = {{1.f/256, 4.f/256, 6.f/256, 4.f/256, 1.f/256},
                        {4.f/256, 16.f/256, 24.f/256, 16.f/256, 4.f/256},
                        {6.f/256, 24.f/256, 36.f/256, 24.f/256, 6.f/256},
                        {4.f/256, 16.f/256, 24.f/256, 16.f/256, 4.f/256},
                        {1.f/256, 4.f/256, 6.f/256, 4.f/256, 1.f/256}};
                bitmap = BitmapModifier.convolution(bitmap, gauss, 5);
                refreshView();
            }
        });

    }

    public void refreshView()
    {
        ImageView image = (ImageView) findViewById(R.id.image);
        image.setImageBitmap(bitmap);
    }
}
